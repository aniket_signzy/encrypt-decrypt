const EnycText = require('../model/encytext');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(`${process.env.SECRET_KEY}`);
const actual_string = "This is actual string";

async function routes(fastify,options){
    fastify.get('/senddata',async(req,res)=>{
        try{

            
            const encryptedString = cryptr.encrypt(actual_string);
            console.log(encryptedString);

            let text = new EnycText();
            text.enyctext = encryptedString;

            await text.save((err)=>{
                if(err)
                {
                    console.log(err);
                }
                else{

                    res.status(200).send({
                        msg : "Succesfully saved to DB"
                    });
    
                }
            });
        }catch{

            res.status(500).send({
                error : true
            })

        }
     
    });

    fastify.get('/verify',async(req,res)=>{

        try {
    
        const enycvalue = await EnycText.find();
        console.log(req.headers);
        //console.log(enycvalue[0].enyctext);

        if(enycvalue)
        {
            
            const decryptedString = cryptr.decrypt(enycvalue[0].enyctext);
            console.log(decryptedString);

            if(decryptedString === req.headers.authorization)
            {
                res.status(200).send({
                    msg : "String matched"
                })
            }
            else{
                res.status(200).send({
                    msg : "String didn't matched"
                })

            }
        }
            
        } catch (error) {

            res.send(500)
            console.log(error);
            
        }
    });
};


module.exports = routes;